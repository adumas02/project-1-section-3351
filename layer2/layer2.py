# Import Layers
import sys
sys.path.append('../')
import mocklayer1 as Layer1
import logging

class StubLayer2:
    def __init__(self, layer_3_cb):
        """Create a Layer 2 Object.

        `layer_3_cb` - Function. This layer will use `layer_3_cb` to pass data to Layer 3. `layer_3_cb`
                       must accept a single parameter `data`.
        """
        # Save parameter inputs as instance attributes so they can be referred to later
        self.layer_3_cb = layer_3_cb

        # Connect to Layer 1 on interface 1. Your actual implementation will
        # have multiple interfaces.
        self.layer1_interface_1 = Layer1.MockLayer1(interface_number=1, layer_2_cb=self.from_layer_1)

    def from_layer_3(self, data, interface):
        """Call this function to send data to this layer from layer 3"""
        logging.debug(f"Layer 2 received msg from Layer 3: {data}")
        
        # Transform data received from layer 3
        data_bits = bin(int.from_bytes(data.encode(), 'big'))[2:]
        while (len(data_bits) % 8 != 0):
            data_bits += '0'

        # calculates parity
        p = parity(data_bits)

        # header + footer to know where layer 2 ends
        footer = '010101010101' # 12 bits
        header = '101010101010' # 12 bits

        # makes string to send to layer 1
        data = header + p + data_bits + footer

        # Layer 3 is making the decision about which interface to send the data. Layer 2
        # simply follows Layer 3's directions (and receives the interface number as an
        # argument.
        datalist = list(data)

        # Pass the message down to Layer 1. This 
        self.layer1_interface_1.from_layer_2(datalist) 

    def from_layer_1(self, data):
        """Call this function to send data to this layer from layer 1"""
        logging.debug(f"Layer 2 received msg from Layer 1: {data}")

        # The big goal of Layer 2 is to take incoming bits from Layer 1 and convert them into
        # Layer 3 messages. This is where you will put the logic for that. Think about:
        # 1. How will you know if the bits are actually part of a message, or just noise?
        # 2. How will you know when a message (a Layer 2 frame) begins?
        # 3. How will you know when a message ends?
        # 4. You will almost certainly *not* receive an entire frame at once. You will
        #    need to save incoming data in some kind of buffer (variable).
        # 5. How will you detect errors? What will you do if you detect an error?

        # make data stream
        data_str = "".join(data)

        # header + footer to know where layer 2 ends
        footer = '010101010101' # 12 bits
        header = '101010101010' # 12 bits
        
        data_header = data_str[:12]
        parity_recv = data_str[12:21]
        data_bits = data_str[21:(len(data_str)-12)]
        data_footer = data_str[-12:]    
        parity_calc = parity(data_bits)
        if (parity_calc != parity_recv):
            return

        bytes_num = int(len(data_bits)/8)
        data = str(int(data_bits, 2).to_bytes(bytes_num, 'big'))
        data_bits = bin(int.from_bytes(data.encode(), 'big'))[2:]

        # Let's just forward this up to layer 3.
        self.layer_3_cb(data)


 # takes a message(in bits) with headers and does a parity check, returning 9 bits of the following format:
    # x x x x x x x x x
    # ^ |             |
    # |  \           /
    # |    ---------
    # |     each bit is comparison of an index in each byte of data
    # |
    # whole bitstream check
    #
    # Ex:
    #   bitstream: 111100000110010000100110
    #   total parity bit = 0
    #
    #                   1 | 1 | 1 | 1 | 0 | 0 | 0 | 0
    #                   0 | 1 | 1 | 0 | 0 | 1 | 0 | 0
    #                   0 | 0 | 1 | 0 | 0 | 1 | 1 | 0
    #                  -------------------------------
    # parity by column: 1   0   1   1   0   0   1   0
    # result = 0 1 0 1 1 0 0 1 0  
    def parity(msg):

        # get first bit
        count = 0
        for i in range(0, len(msg)):
            if msg[i] == 1:
                count += 1
        count %= 2
        parity_bits = ''
        if count == 0:
            parity_bits += '0'
        else:
            parity_bits += '1'
        # get all bytes in 2d array
        # byte_split = [[0 for x in range(len(msg)/8)], 8]
        
        # splits into bytes
        byte_array = []
        num_bytes = int(len(msg)/8)
        for x in range(len(num_bytes)):
            i= x*8
            byte_array[i] = msg[i:i+8]
        
        parity_arr = []
        for x in range(len(byte_array)):
            for i in range(8):
                if (byte_array[i] == 1):
                    parity_arr[i] += 1
        
        for x in range(len(parity_arr)):
            if (parity_arr[i] % 2 == 0):
                parity_bits += '0'
            else:
                parity_bits += '1'

        return parity_bits
